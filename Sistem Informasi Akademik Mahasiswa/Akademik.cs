﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistem_Informasi_Akademik_Mahasiswa
{
    public partial class FormAkademik : Form
    {

        //Attribute
        private string connectionString;
        private string query;
        private MySqlConnection databaseConnection;
        private MySqlCommand commandDatabase;
        private String nim;
        //private object openFileDialogImage;

        // sugab
        FormJadwal jadwal;
        FormKartuHasilStudi khs;//= new FormKartuHasilStudiAdmin(nim);
        FormKartuHasilStudiAdmin khsA;
        FormMataKuliah matkul;
        Mahasiswa mhs;
        Kelas kelas;
        KartuRencanaStudi krs;


        public FormAkademik(String nim)
        {
            InitializeComponent();
            linkLabelMataKuliah.Visible = false;
            linkLabelMahasiswa.Visible = false;
            linkLabelKelas.Visible = false;
            this.nim = nim;

            jadwal = new FormJadwal(nim);

            if (nim == "admin")
            {
                // sugab
                
                /*khsA = new FormKartuHasilStudiAdmin(nim);
                matkul = new FormMataKuliah(nim);
                mhs = new Mahasiswa();
                kelas = new Kelas(nim);*/

                linkLabelMataKuliah.Visible = true;
                linkLabelMahasiswa.Visible = true;
                linkLabelKelas.Visible = true;

                //sugab
                LabelJurusan.Visible = false;
                labelSeleksi.Visible = false;
                Labelstatus.Visible = false;
                Status.Visible = false;
                labelNim.Text   = "Administrator";
                labelNama.Text  = "Administrator";

                
                connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=siam;";
                query = "SELECT foto_profil FROM administrator WHERE username ='admin' "; ;

                // Prepare the connection
                databaseConnection = new MySqlConnection(connectionString);
                commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                MySqlDataReader reader;

                // Open the database
                databaseConnection.Open();

                // Execute the query
                reader = commandDatabase.ExecuteReader();
                string phhoto;
                
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        phhoto = reader.GetString(0);
                        if (phhoto != null)
                            pictureBoxFoto.Image = imgFromBase64(phhoto);
                    }
                }
            }
            else
            {
                // sugab
                mahasiswa();
            }
        }
    
        private void mahasiswa()
        {
            // Change the username, password and database according to your needs
            // You can ignore the database option if you want to access all of them.
            // 127.0.0.1 stands for localhost and the default port to connect.
            connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=siam;";
            // Your query,
            query = "SELECT * FROM mahasiswa WHERE nim = '" + nim + "'"; ;

            // Prepare the connection
            databaseConnection = new MySqlConnection(connectionString);
            commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            // Let's do it !
            try
            {
                // Open the database
                databaseConnection.Open();

                // Execute the query
                reader = commandDatabase.ExecuteReader();

                // All succesfully executed, now do something

                // IMPORTANT : 
                // If your query returns result, use the following processor :

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // As our database, the array will contain : ID 0, NIM 1,PASSWORD 2, FULLNAME 3, FAKULTAS 4, JURUSAN 5, SELEKSI 6, STATUS 7
                        // Do something with every received database ROW
                        string[] row = { reader.GetString(1), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7), reader.GetString(8)};
                        labelNim.Text = row[0];
                        labelNama.Text = row[1];
                        labelFakultas.Text  = "Degree/Faculty > S1/ " + row[2];
                        LabelJurusan.Text   = "Department > " + row[3];
                        labelSeleksi.Text   = "Selection > " + row[4];
                        Labelstatus.Text    = row[5];
                        if(row[6] != null)
                        {
                            pictureBoxFoto.Image = imgFromBase64(row[6]);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

                // Finally close the connection
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Show any error message.
                MessageBox.Show(ex.Message);
            }
        }

        private void linkLabelMahasiswa_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //sugab
            if ((mhs == null)|| (!mhs.IsHandleCreated))
                mhs = new Mahasiswa();
           
            mhs.MdiParent = this;
            mhs.Show();
            mhs.BringToFront();
        }

        private void linkLabelKartuRencanaStudi_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //KartuRencanaStudi krs = new KartuRencanaStudi(nim);
            if( (krs == null ) || (!krs.IsHandleCreated))
                krs = new KartuRencanaStudi(nim);
            krs.MdiParent = this;
            krs.Show();
            krs.BringToFront();
        }

        private void linkLabelJadwalKuliah_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //FormJadwal jadwal = new FormJadwal(nim);
            // sugab
            
            jadwal.MdiParent = this;
            jadwal.Show();
            jadwal.BringToFront();
        }

        private void linkLabelKartuHasilStudi_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if(nim == "admin")
            {
                //sugab
                //FormKartuHasilStudiAdmin khs = new FormKartuHasilStudiAdmin(nim);

                if ((khsA == null) || (!khsA.IsHandleCreated))
                    khsA = new FormKartuHasilStudiAdmin(nim);
                khsA.MdiParent = this;
                khsA.Show();
                khsA.BringToFront();
            }
            else
            {
                //FormKartuHasilStudi khs = new FormKartuHasilStudi(nim);
                if ((khs == null) || (!khs.IsHandleCreated))
                    khs = new FormKartuHasilStudi(nim);
                khs.MdiParent = this;
                khs.Show();
                khs.BringToFront();
            }
        }

        private void FormAkademik_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DestroyHandle();
            //Application.Exit();
        }

        private void linkLabelMataKuliah_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //sugab
            //FormMataKuliah matkul = new FormMataKuliah(nim);
            if((matkul == null)  || (!matkul.IsHandleCreated))
                matkul = new FormMataKuliah(nim);
            matkul.MdiParent = this;
            matkul.Show();
            //sugab
            matkul.BringToFront();
        }

        private void buttonUbahFoto_Click(object sender, EventArgs e)
        {
            openFileDialogFoto.ShowDialog();
            string path = openFileDialogFoto.FileName;
            //Checking file size
            if(openFileDialogFoto.FileName.Length > 307200)
            {
                MessageBox.Show("Photo size file are too big");
                return;
            }

            //Image to Base64
            using (Image image = Image.FromFile(path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    //return base64String;
                    pictureBoxFoto.Image = imgFromBase64(base64String);
                    updateImage(base64String);
                }
            }
        }

        private void updateImage(string base64)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=siam;";
            string query;

            if (nim == "admin")
                query = "UPDATE `administrator` SET `foto_profil`='" + base64 + "' WHERE `username`='" + nim + "'";
            else
                query = "UPDATE `mahasiswa` SET `foto_profil`='" + base64 + "' WHERE `nim`='" + labelNim.Text + "'";
            

            // Update the properties of the row with ID 1
            //string query = "UPDATE `mahasiswa` SET `foto_profil`='" + base64 + "' WHERE `nim`='" + labelNim.Text + "'";


            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                // Succesfully updated
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Ops, maybe the id doesn't exists ?
                MessageBox.Show(ex.Message);
            }
        }
    
        private Image imgFromBase64(string base64)
        {
            Image image;
            using (var ms = new MemoryStream(Convert.FromBase64String(base64)))
            {
                image = Image.FromStream(ms, true);
                //pictureBox1.Image = image;
            }
            return image;
        }

        private void linkLabelNilai_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //sugab
            //Kelas kelas = new Kelas("admin");
            if ((kelas == null) || (!kelas.IsHandleCreated))
                kelas = new Kelas(nim);
            kelas.MdiParent = this;
            kelas.Show();
            kelas.BringToFront();
        }

        private void buttonKeluar_Click(object sender, EventArgs e)
        {
            this.Close();
            //FormLogin form = new FormLogin();
            //form.Show();
        }
    }
}
